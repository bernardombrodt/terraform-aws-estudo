provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
}

#instancias automaticas 3 unidades sem bucket
resource "aws_instance" "dev" {
  count = 3
  ami = "ami-0bcc094591f354be2"
  instance_type = "t2.micro"
  key_name = "terraform-aws"
  tags = {
    Name = "dev${count.index}"
  }
  vpc_security_group_ids = ["${aws_security_group.acesso-ssh.id}"]
}
#Grupo de segurança
resource "aws_security_group" "acesso-ssh" {
  name        = "acesso-ssh"
  description = "acesso-ssh"

  ingress {
      from_port = 22
      to_port   = 22
      protocol  ="tcp"

      cidr_blocks = ["189.6.238.120/32"]

  }
  tags = {
      Name = "ssh"
  }
  }
  #Instancia unitaria com bucket
  resource "aws_instance" "dev4" {
      ami = "ami-0bcc094591f354be2"
      instance_type = "t2.micro"
      key_name = "terraform-aws"
      tags = {
          Name = "dev4"
      }
      vpc_security_group_ids = ["${aws_security_group.acesso-ssh.id}"]  
      depends_on = ["aws_s3_bucket.dev4"]     
  }
  resource "aws_instance" "dev5" {
      ami = "ami-0bcc094591f354be2"
      instance_type = "t2.micro"
      key_name = "terraform-aws"
      tags = {
          Name = "dev5"
      }
      vpc_security_group_ids = ["${aws_security_group.acesso-ssh.id}"]  
      depends_on = ["aws_s3_bucket.dev4"]     
  }

  #------------Adc bucket s3
  resource "aws_s3_bucket" "dev4" {
      bucket = "rmerceslabs-dev"
      acl    = "private"

      tags = {
          Name = "rmerceslabs-dev"
      }

  }
  